

import java.io.*;
import java.util.*;

public class Task1 {

    public static void main(String[] args) throws IOException {
        while (true) {
            System.out.println("Создать текстовый файл с перемешанными числовыми значениями от 0 до 20 (введите 1)");
            System.out.println("Отсортировать числа в порядке возрастания (введите 2)");
            System.out.println("Отсортировать числа в порядке убывания (введите 3)");
            int input = new Scanner(System.in).nextInt();
            if (input == 1) {
                try(FileWriter writer = new FileWriter("numbers.txt", false))
                {
                    List<Integer> numbers = new ArrayList<>();
                    for (int i = 0; i < 21; i++) {
                        numbers.add(i);
                    }
                    Collections.shuffle(numbers);

                    for(Integer number : numbers){
                        writer.write(String.valueOf(number));
                        writer.write(',');
                    }

                    writer.flush();
                }
                catch(IOException ex){

                    System.out.println(ex.getMessage());
                }
                System.out.println("Файл numbers.txt создан рядом с исполняемым файлом.");
            } else if (input == 2) {
                List<Integer> numbers = ReadNumbers();
                for (int i = 0; i < numbers.size(); i++) {
                    for (int j = 0; j < numbers.size(); j++) {
                        if (numbers.get(i) < numbers.get(j)) {
                            int swap = numbers.get(j);
                            numbers.set(j, numbers.get(i));
                            numbers.set(i, swap);
                        }
                    }
                }
                System.out.println("Ответ: ");
                for (Integer num : numbers) {
                    System.out.print(num + " ");
                }
                System.out.println();

            } else {
                List<Integer> numbers = ReadNumbers();
                for (int i = 0; i < numbers.size(); i++) {
                    for (int j = 0; j < numbers.size(); j++) {
                        if (numbers.get(i) > numbers.get(j)) {
                            int swap = numbers.get(j);
                            numbers.set(j, numbers.get(i));
                            numbers.set(i, swap);
                        }
                    }
                }
                System.out.println("Ответ: ");
                for (Integer num : numbers) {
                    System.out.print(num + " ");
                }
                System.out.println();
            }
        }
    }

    public static List<Integer> ReadNumbers() throws IOException {
        try(BufferedReader br = new BufferedReader(new FileReader("numbers.txt"))) {
            StringBuilder sb = new StringBuilder();
            String line = br.readLine();

            while (line != null) {
                sb.append(line);
                line = br.readLine();
            }
            String[] text = sb.toString().split(",");
            List<Integer> numbers = new ArrayList<>();
            for (String num : text) {
                numbers.add(Integer.parseInt(num));
            }
            for (int i = 0; i < numbers.size(); i++) {
                for (int j = 0; j < numbers.size(); j++) {
                    if (numbers.get(i) < numbers.get(j)) {
                        int swap = numbers.get(j);
                        numbers.set(j, numbers.get(i));
                        numbers.set(i, swap);
                    }
                }
            }
            return numbers;
        }
    }



}
